﻿namespace Github.Net
{
    public enum LoginState : byte
    {
        LoggedOut,
        LoggingIn,
        LoggedIn,
        LoggingOut
    }
}
