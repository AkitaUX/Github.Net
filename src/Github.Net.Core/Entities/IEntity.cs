﻿using System;

namespace Github.Net
{
    public interface IEntity<TId>
        where TId : IEquatable<TId>
    {
        /// <summary> Gets the IClient that created this object. </summary>
        IGithubClient Github { get; }
        /// <summary> Gets the unique identifier for this object. </summary>
        TId Id { get; }
    }
}
