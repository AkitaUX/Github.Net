﻿using System.Threading.Tasks;

namespace Github.Net
{
    public interface IGithubClient
    {
        ConnectionState ConnectionState { get; }

        Task StartAsync();
        Task StopAsync();
    }
}
