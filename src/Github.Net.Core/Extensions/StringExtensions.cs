﻿using System;
using System.Text;

namespace Github.Net
{
    public static class StringExtensions
    {
        public static string ToBase64String(this string text)
        {
            var bytes = Encoding.GetEncoding(GithubConfig.Encoding).GetBytes(text);
            return Convert.ToBase64String(bytes);
        }
    }
}
