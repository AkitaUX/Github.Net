﻿using Github.Net;
using Newtonsoft.Json;
using System;

namespace Github.API
{
    internal class Authorization
    {
        [JsonProperty("id")]
        public Optional<ulong> Id { get; set; }
        [JsonProperty("url")]
        public Optional<string> Url { get; set; }
        [JsonProperty("scopes")]
        public Optional<string[]> Scopes { get; set; }
        [JsonProperty("token")]
        public Optional<string> Token { get; set; }
        [JsonProperty("token_last_eight")]
        public Optional<int> TokenLastEight { get; set; }
        [JsonProperty("hashed_token")]
        public Optional<string> HashedToken { get; set; }
        [JsonProperty("app")]
        public Optional<Application> Application { get; set; }
        [JsonProperty("note")]
        public Optional<string> Note { get; set; }
        [JsonProperty("note_url")]
        public Optional<string> NoteUrl { get; set; }
        [JsonProperty("created_at")]
        public Optional<DateTime> CreatedAt { get; set; }
        [JsonProperty("updated_at")]
        public Optional<DateTime> UpdatedAt { get; set; }
        [JsonProperty("fingerprint")]
        public Optional<string> Fingerprint { get; set; }
    }
}
