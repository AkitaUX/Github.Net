﻿using System;
using System.Collections.Generic;
using Github.Net;
using Newtonsoft.Json;

namespace Github.API
{
    internal class Repository
    {
        // Simple Repository
        [JsonProperty("id")]
        public ulong Id { get; set; }
        [JsonProperty("owner")]
        public User Owner { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("full_name")]
        public string FullName { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("private")]
        public bool IsPrivate { get; set; }
        [JsonProperty("fork")]
        public bool IsFork { get; set; }
        [JsonProperty("html_url")]
        public string Url { get; set; }

        // Full Repository
        [JsonProperty("language")]
        public Optional<string> Language { get; set; }
        [JsonProperty("forks_count")]
        public Optional<int> ForksCount { get; set; }
        [JsonProperty("stargazers_count")]
        public Optional<int> StargazersCount { get; set; }
        [JsonProperty("watchers_count")]
        public Optional<int> WatchersCount { get; set; }
        [JsonProperty("size")]
        public Optional<int> Size { get; set; }
        [JsonProperty("default_branch")]
        public Optional<string> DefaultBranch { get; set; }
        [JsonProperty("open_issues_count")]
        public Optional<int> OpenIssuesCount { get; set; }
        [JsonProperty("topics")]
        public Optional<IReadOnlyCollection<string>> Topics { get; set; }
        [JsonProperty("has_issues")]
        public Optional<bool> HasIssues { get; set; }
        [JsonProperty("has_wiki")]
        public Optional<bool> HasWiki { get; set; }
        [JsonProperty("has_pages")]
        public Optional<bool> HasPages { get; set; }
        [JsonProperty("has_downloads")]
        public Optional<bool> HasDownloads { get; set; }
        [JsonProperty("archived")]
        public Optional<bool> IsArchived { get; set; }
        [JsonProperty("pushed_at")]
        public Optional<DateTime> PushedAt { get; set; }
        [JsonProperty("created_at")]
        public Optional<DateTime> CreatedAt { get; set; }
        [JsonProperty("updated_at")]
        public Optional<DateTime> UpdatedAt { get; set; }
        [JsonProperty("permissions")]
        public Optional<RepositoryPermission> Permissions { get; set; }
        [JsonProperty("allow_rebase_merge")]
        public Optional<bool> AllowRebaseMerge { get; set; }
        [JsonProperty("allow_squash_merge")]
        public Optional<bool> AllowSquashMerge { get; set; }
        [JsonProperty("allow_merge_commit")]
        public Optional<bool> AllowMergeCommit { get; set; }
        [JsonProperty("subscribers_count")]
        public Optional<int> SubscribersCount { get; set; }
        [JsonProperty("network_count")]
        public Optional<int> NetworkCount { get; set; }

        // Created Response Repository
        [JsonProperty("has_projects")]
        public Optional<bool> HasProjects { get; set; }

        // Endpoint Properties
        [JsonProperty("url")]
        public Optional<string> RepoUrl { get; set; }
        [JsonProperty("archive_url")]
        public Optional<string> ArchiveUrl { get; set; }
        [JsonProperty("assignees_url")]
        public Optional<string> AssigneesUrl { get; set; }
        [JsonProperty("blobs_url")]
        public Optional<string> BlobsUrl { get; set; }
        [JsonProperty("branches_url")]
        public Optional<string> BranchesUrl { get; set; }
        [JsonProperty("clone_url")]
        public Optional<string> CloneUrl { get; set; }
        [JsonProperty("collaborators_url")]
        public Optional<string> CollaboratorsUrl { get; set; }
        [JsonProperty("comments_url")]
        public Optional<string> CommentsUrl { get; set; }
        [JsonProperty("commits_url")]
        public Optional<string> CommitsUrl { get; set; }
        [JsonProperty("compare_url")]
        public Optional<string> CompareUrl { get; set; }
        [JsonProperty("contents_url")]
        public Optional<string> ContentsUrl { get; set; }
        [JsonProperty("contributors_url")]
        public Optional<string> ContributorsUrl { get; set; }
        [JsonProperty("deployments_url")]
        public Optional<string> DeploymentsUrl { get; set; }
        [JsonProperty("downloads_url")]
        public Optional<string> DownloadsUrl { get; set; }
        [JsonProperty("events_url")]
        public Optional<string> EventsUrl { get; set; }
        [JsonProperty("forks_url")]
        public Optional<string> ForksUrl { get; set; }
        [JsonProperty("git_commits_url")]
        public Optional<string> GitCommitsUrl { get; set; }
        [JsonProperty("git_refs_url")]
        public Optional<string> GitRefsUrl { get; set; }
        [JsonProperty("git_tags_url")]
        public Optional<string> GitTagsUrl { get; set; }
        [JsonProperty("git_url")]
        public Optional<string> GitUrl { get; set; }
        [JsonProperty("hooks_url")]
        public Optional<string> HooksUrl { get; set; }
        [JsonProperty("issue_comment_url")]
        public Optional<string> IssueCommentUrl { get; set; }
        [JsonProperty("issue_events_url")]
        public Optional<string> IssueEventsUrl { get; set; }
        [JsonProperty("issues_url")]
        public Optional<string> IssuesUrl { get; set; }
        [JsonProperty("keys_url")]
        public Optional<string> KeysUrl { get; set; }
        [JsonProperty("labels_url")]
        public Optional<string> LabelsUrl { get; set; }
        [JsonProperty("languages_url")]
        public Optional<string> LanguagesUrl { get; set; }
        [JsonProperty("merges_url")]
        public Optional<string> MergesUrl { get; set; }
        [JsonProperty("milestones_url")]
        public Optional<string> MilestonesUrl { get; set; }
        [JsonProperty("mirror_url")]
        public Optional<string> MirrorUrl { get; set; }
        [JsonProperty("notifications_url")]
        public Optional<string> NotificationsUrl { get; set; }
        [JsonProperty("pulls_url")]
        public Optional<string> PullsUrl { get; set; }
        [JsonProperty("releases_url")]
        public Optional<string> ReleasesUrl { get; set; }
        [JsonProperty("ssh_url")]
        public Optional<string> SshUrl { get; set; }
        [JsonProperty("stargazers_url")]
        public Optional<string> StargazersUrl { get; set; }
        [JsonProperty("statuses_url")]
        public Optional<string> StatusesUrl { get; set; }
        [JsonProperty("subscribers_url")]
        public Optional<string> SubscribersUrl { get; set; }
        [JsonProperty("subscription_url")]
        public Optional<string> SubscriptionUrl { get; set; }
        [JsonProperty("svn_url")]
        public Optional<string> SvnUrl { get; set; }
        [JsonProperty("tags_url")]
        public Optional<string> TagsUrl { get; set; }
        [JsonProperty("teams_url")]
        public Optional<string> TeamsUrl { get; set; }
        [JsonProperty("trees_url")]
        public Optional<string> TreesUrl { get; set; }
        [JsonProperty("homepage")]
        public Optional<string> HomepageUrl { get; set; }
    }
}
