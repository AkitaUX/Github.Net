﻿using Newtonsoft.Json;

namespace Github.API
{
    internal class RepositoryLicense
    {
        [JsonProperty("key")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("spdx_id")]
        public string SpdxId { get; set; }
        [JsonProperty("html_url")]
        public string Url { get; set; }

        // Endpoint Properties
        [JsonProperty("url")]
        public string LicenseUrl { get; set; }
    }
}
