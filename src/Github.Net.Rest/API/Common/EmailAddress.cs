﻿using Github.Net;
using Newtonsoft.Json;

namespace Github.API
{
    internal class EmailAddress
    {
        [JsonProperty("email")]
        public Optional<string> Email { get; set; }
        [JsonProperty("verified")]
        public Optional<bool> IsVerified { get; set; }
        [JsonProperty("primary")]
        public Optional<bool> IsPrimary { get; set; }
        [JsonProperty("visibility")]
        public Optional<string> Visibility { get; set; }
    }
}
