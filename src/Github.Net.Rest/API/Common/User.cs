﻿using Github.Net;
using Newtonsoft.Json;
using System;

namespace Github.API
{
    internal class User
    {
        // User
        [JsonProperty("id")]
        public ulong Id { get; set; }
        [JsonProperty("login")]
        public Optional<string> Username { get; set; }
        [JsonProperty("avatar_url")]
        public Optional<string> AvatarUrl { get; set; }
        [JsonProperty("gravatar_id")]
        public Optional<string> GravatarId { get; set; }
        [JsonProperty("html_url")]
        public Optional<string> Url { get; set; }
        [JsonProperty("type")]
        public Optional<string> Type { get; set; }
        [JsonProperty("site_admin")]
        public Optional<bool> IsSiteAdmin { get; set; }

        // ProfileUser
        [JsonProperty("name")]
        public Optional<string> Name { get; set; }
        [JsonProperty("company")]
        public Optional<string> Company { get; set; }
        [JsonProperty("blog")]
        public Optional<string> BlogUrl { get; set; }
        [JsonProperty("location")]
        public Optional<string> Location { get; set; }
        [JsonProperty("email")]
        public Optional<string> Email { get; set; }
        [JsonProperty("hireable")]
        public Optional<bool> IsHireable { get; set; }
        [JsonProperty("bio")]
        public Optional<string> Description { get; set; }
        [JsonProperty("public_repos")]
        public Optional<int> PublicReposCount { get; set; }
        [JsonProperty("public_gists")]
        public Optional<int> PublicGistsCount { get; set; }
        [JsonProperty("followers")]
        public Optional<int> FollowersCount { get; set; }
        [JsonProperty("following")]
        public Optional<int> FollowingCount { get; set; }
        [JsonProperty("created_at")]
        public Optional<DateTime> CreatedAt { get; set; }
        [JsonProperty("updated_at")]
        public Optional<DateTime> UpdatedAt { get; set; }

        // SelfUser
        [JsonProperty("total_private_repos")]
        public Optional<int> PrivateReposCount { get; set; }
        [JsonProperty("owned_private_repos")]
        public Optional<int> OwnedPrivateReposCount { get; set; }
        [JsonProperty("private_gists")]
        public Optional<int> PrivateGistsCount { get; set; }
        [JsonProperty("disk_usage")]
        public Optional<int> DiskUsage { get; set; }
        [JsonProperty("collaborators")]
        public Optional<int> CollaboratorsCount { get; set; }
        [JsonProperty("two_factor_authentication")]
        public Optional<bool> IsTwoFactorAuthenticated { get; set; }
        [JsonProperty("plan")]
        public Optional<Plan> Plan { get; set; }
        
        // Endpoint Properties
        [JsonProperty("url")]
        public Optional<string> UserUrl { get; set; }
        [JsonProperty("followers_url")]
        public Optional<string> FollowersUrl { get; set; }
        [JsonProperty("following_url")]
        public Optional<string> FollowingUrl { get; set; }
        [JsonProperty("gists_url")]
        public Optional<string> GistsUrl { get; set; }
        [JsonProperty("starred_url")]
        public Optional<string> StarredUrl { get; set; }
        [JsonProperty("subscriptions_url")]
        public Optional<string> SubscriptionsUrl { get; set; }
        [JsonProperty("organizations_url")]
        public Optional<string> OrganizationsUrl { get; set; }
        [JsonProperty("repos_url")]
        public Optional<string> ReposUrl { get; set; }
        [JsonProperty("events_url")]
        public Optional<string> EventsUrl { get; set; }
        [JsonProperty("received_events_url")]
        public Optional<string> ReceivedEventsUrl { get; set; }
    }
}
