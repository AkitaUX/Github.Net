﻿using Github.Net;
using Newtonsoft.Json;

namespace Github.API
{
    internal class Application
    {
        [JsonProperty("url")]
        public Optional<string> Url { get; set; }
        [JsonProperty("name")]
        public Optional<string> Name { get; set; }
        [JsonProperty("client_id")]
        public Optional<string> ClientId { get; set; }
    }
}
