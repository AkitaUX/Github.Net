﻿using Newtonsoft.Json;

namespace Github.API
{
    internal class RepositoryPermission
    {
        [JsonProperty("admin")]
        public bool IsAdmin { get; set; }
        [JsonProperty("push")]
        public bool CanPush { get; set; }
        [JsonProperty("pull")]
        public bool CanPull { get; set; }
    }
}
