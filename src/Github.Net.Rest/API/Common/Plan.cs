﻿using Github.Net;
using Newtonsoft.Json;

namespace Github.API
{
    internal class Plan
    {
        [JsonProperty("name")]
        public Optional<string> Name { get; set; }
        [JsonProperty("space")]
        public Optional<int> Space { get; set; }
        [JsonProperty("private_repos")]
        public Optional<int> PrivateReposCount { get; set; }
        [JsonProperty("collaborators")]
        public Optional<int> CollaboratorsCount { get; set; }
    }
}
