﻿using Github.Net;
using Newtonsoft.Json;
using System;

namespace Github.API
{
    internal class Grant
    {
        [JsonProperty("id")]
        public Optional<ulong> Id { get; set; }
        [JsonProperty("url")]
        public Optional<string> Url { get; set; }
        [JsonProperty("app")]
        public Optional<Application> Application { get; set; }
        [JsonProperty("created_at")]
        public Optional<DateTime> CreatedAt { get; set; }
        [JsonProperty("updated_at")]
        public Optional<DateTime> UpdatedAt { get; set; }
        [JsonProperty("scopes")]
        public Optional<string[]> Scopes { get; set; }
    }
}
