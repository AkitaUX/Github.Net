﻿using Github.Net;
using Newtonsoft.Json;

namespace Github.API
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    internal class CreateAuthorizationParams
    {
        [JsonProperty("scopes")]
        public Optional<string[]> Scopes { get; set; }
        [JsonProperty("note")]
        public Optional<string> Note { get; set; }
        [JsonProperty("note_url")]
        public Optional<string> NoteUrl { get; set; }
        [JsonProperty("client_id")]
        public Optional<string> ClientId { get; set; }
        [JsonProperty("client_secret")]
        public Optional<string> ClientSecret { get; set; }
        [JsonProperty("fingerprint")]
        public Optional<string> Fingerprint { get; set; }
    }
}
