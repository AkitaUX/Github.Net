﻿using Github.Net;
using Newtonsoft.Json;

namespace Github.API.Rest
{
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    internal class ModifyUserParams
    {
        [JsonProperty("name")]
        public Optional<string> Name { get; set; }
        [JsonProperty("email")]
        public Optional<string> Email { get; set; }
        [JsonProperty("blog")]
        public Optional<string> BlogUrl { get; set; }
        [JsonProperty("company")]
        public Optional<string> Company { get; set; }
        [JsonProperty("location")]
        public Optional<string> Location { get; set; }
        [JsonProperty("hireable")]
        public Optional<bool> IsHireable { get; set; }
        [JsonProperty("bio")]
        public Optional<string> Description { get; set; }
    }
}
