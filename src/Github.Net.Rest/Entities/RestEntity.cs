﻿using System;

namespace Github.Net.Rest
{
    public abstract class RestEntity<T> : IEntity<T>
        where T : IEquatable<T>
    {
        internal BaseGithubClient Github { get; }
        public T Id { get; }

        IGithubClient IEntity<T>.Github => Github;
        
        internal RestEntity(BaseGithubClient github, T id)
        {
            Github = github;
            Id = id;
        }
    }
}
