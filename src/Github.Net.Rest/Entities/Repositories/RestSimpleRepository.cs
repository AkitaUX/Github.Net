﻿using System;
using System.Threading.Tasks;
using Model = Github.API.Repository;

namespace Github.Net.Rest
{
    public class RestSimpleRepository : RestEntity<ulong>, IUpdateable
    {
        public RestSimpleUser Owner { get; private set; }
        public string Name { get; private set; }
        public string FullName { get; private set; }
        public string Description { get; private set; }
        public bool IsPrivate { get; private set; }
        public bool IsFork { get; private set; }
        public string Url { get; private set; }

        internal RestSimpleRepository(BaseGithubClient github, ulong id)
            : base(github, id) { }
        internal static RestSimpleRepository Create(BaseGithubClient github, Model model)
        {
            var entity = new RestSimpleRepository(github, model.Id);
            entity.Update(model);
            return entity;
        }
        internal virtual void Update(Model model)
        {
            Owner = RestSimpleUser.Create(Github, model.Owner);
            Name = model.Name;
            FullName = model.FullName;
            Description = model.Description;
            IsPrivate = model.IsPrivate;
            IsFork = model.IsFork;
            Url = model.Url;
        }

        public Task UpdateAsync(RequestOptions options = null)
        {
            throw new NotImplementedException();
        }

        public override string ToString() => Name;
    }
}
