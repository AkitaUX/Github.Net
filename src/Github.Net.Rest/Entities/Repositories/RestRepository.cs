﻿using System;
using System.Collections.Generic;
using Model = Github.API.Repository;

namespace Github.Net.Rest
{
    public class RestRepository : RestSimpleRepository
    {
        public string Language { get; private set; }
        public int ForksCount { get; private set; }
        public int StargazersCount { get; private set; }
        public int WatchersCount { get; private set; }
        public int Size { get; private set; }
        public string DefaultBranch { get; private set; }
        public int OpenIssuesCount { get; private set; }
        public IReadOnlyCollection<string> Topics { get; private set; }
        public bool HasIssues { get; private set; }
        public bool HasWiki { get; private set; }
        public bool HasPages { get; private set; }
        public bool HasDownloads { get; private set; }
        public bool IsArchived { get; private set; }
        public DateTime PushedAt { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        //public RepositoryPermission Permissions { get; private set; }
        public bool AllowRebaseMerge { get; private set; }
        public bool AllowSquashMerge { get; private set; }
        public bool AllowMergeCommit { get; private set; }
        public int SubscribersCount { get; private set; }
        public int NetworkCount { get; private set; }
        public bool? HasProjects { get; private set; }
        
        internal RestRepository(BaseGithubClient github, ulong id)
            : base(github, id) { }
        internal new static RestRepository Create(BaseGithubClient github, Model model)
        {
            var entity = new RestRepository(github, model.Id);
            entity.Update(model);
            return entity;
        }
        internal override void Update(Model model)
        {
            base.Update(model);
            if (model.Language.IsSpecified)
                Language = model.Language.Value;
            if (model.ForksCount.IsSpecified)
                ForksCount = model.ForksCount.Value;
            if (model.StargazersCount.IsSpecified)
                StargazersCount = model.StargazersCount.Value;
            if (model.WatchersCount.IsSpecified)
                WatchersCount = model.WatchersCount.Value;
            if (model.Size.IsSpecified)
                Size = model.Size.Value;
            if (model.DefaultBranch.IsSpecified)
                DefaultBranch = model.DefaultBranch.Value;
            if (model.OpenIssuesCount.IsSpecified)
                OpenIssuesCount = model.OpenIssuesCount.Value;
            if (model.Topics.IsSpecified)
                Topics = model.Topics.Value;
            if (model.HasIssues.IsSpecified)
                HasIssues = model.HasIssues.Value;
            if (model.HasWiki.IsSpecified)
                HasWiki = model.HasWiki.Value;
            if (model.HasPages.IsSpecified)
                HasPages = model.HasPages.Value;
            if (model.HasDownloads.IsSpecified)
                HasDownloads = model.HasDownloads.Value;
            if (model.IsArchived.IsSpecified)
                IsArchived = model.IsArchived.Value;
            if (model.PushedAt.IsSpecified)
                PushedAt = model.PushedAt.Value;
            if (model.CreatedAt.IsSpecified)
                CreatedAt = model.CreatedAt.Value;
            if (model.UpdatedAt.IsSpecified)
                UpdatedAt = model.UpdatedAt.Value;
            //if (model.Permissions.IsSpecified)
            //    Permissions = model.Permissions.Value;
            if (model.AllowRebaseMerge.IsSpecified)
                AllowRebaseMerge = model.AllowRebaseMerge.Value;
            if (model.AllowSquashMerge.IsSpecified)
                AllowSquashMerge = model.AllowSquashMerge.Value;
            if (model.AllowMergeCommit.IsSpecified)
                AllowMergeCommit = model.AllowMergeCommit.Value;
            if (model.SubscribersCount.IsSpecified)
                SubscribersCount = model.SubscribersCount.Value;
            if (model.NetworkCount.IsSpecified)
                NetworkCount = model.NetworkCount.Value;
            if (model.HasProjects.IsSpecified)
                HasProjects = model.HasProjects.Value;
        }
    }
}
