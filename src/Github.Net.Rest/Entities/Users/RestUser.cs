﻿using System;
using Model = Github.API.User;

namespace Github.Net.Rest
{
    public class RestUser : RestSimpleUser
    {
        public string Name { get; private set; }
        public string Company { get; private set; }
        public string BlogUrl { get; private set; }
        public string Location { get; private set; }
        public string Email { get; private set; }
        public bool IsHireable { get; private set; }
        public string Description { get; private set; }
        public int PublicReposCount { get; private set; }
        public int PublicGistsCount { get; private set; }
        public int FollowersCount { get; private set; }
        public int FollowingCount { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        
        internal RestUser(BaseGithubClient github, ulong id) 
            : base(github, id) { }
        internal new static RestUser Create(BaseGithubClient github, Model model)
        {
            var entity = new RestUser(github, model.Id);
            entity.Update(model);
            return entity;
        }
        internal override void Update(Model model)
        {
            base.Update(model);
            if (model.Name.IsSpecified)
                Name = model.Name.Value;
            if (model.Company.IsSpecified)
                Company = model.Company.Value;
            if (model.BlogUrl.IsSpecified)
                BlogUrl = model.BlogUrl.Value;
            if (model.Location.IsSpecified)
                Location = model.Location.Value;
            if (model.Email.IsSpecified)
                Email = model.Email.Value;
            if (model.IsHireable.IsSpecified)
                IsHireable = model.IsHireable.Value;
            if (model.Description.IsSpecified)
                Description = model.Description.Value;
            if (model.PublicReposCount.IsSpecified)
                PublicReposCount = model.PublicReposCount.Value;
            if (model.PublicGistsCount.IsSpecified)
                PublicGistsCount = model.PublicGistsCount.Value;
            if (model.FollowersCount.IsSpecified)
                FollowersCount = model.FollowersCount.Value;
            if (model.FollowingCount.IsSpecified)
                FollowingCount = model.FollowingCount.Value;
            if (model.CreatedAt.IsSpecified)
                CreatedAt = model.CreatedAt.Value;
            if (model.UpdatedAt.IsSpecified)
                UpdatedAt = model.UpdatedAt.Value;
        }
    }
}
