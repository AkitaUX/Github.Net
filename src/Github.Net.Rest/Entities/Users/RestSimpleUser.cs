﻿using System.Threading.Tasks;
using Model = Github.API.User;

namespace Github.Net.Rest
{
    public class RestSimpleUser : RestEntity<ulong>, IUpdateable
    {
        public string Username { get; private set; }
        public string AvatarUrl { get; private set; }
        public string GravatarId { get; private set; }
        public string Url { get; private set; }
        public string Type { get; private set; }
        public bool IsSiteAdmin { get; private set; }

        internal RestSimpleUser(BaseGithubClient github, ulong id) 
            : base(github, id) { }
        internal static RestSimpleUser Create(BaseGithubClient github, Model model)
        {
            var entity = new RestSimpleUser(github, model.Id);
            entity.Update(model);
            return entity;
        }
        internal virtual void Update(Model model)
        {
            if (model.Username.IsSpecified)
                Username = model.Username.Value;
            if (model.AvatarUrl.IsSpecified)
                AvatarUrl = model.AvatarUrl.Value;
            if (model.GravatarId.IsSpecified)
                GravatarId = model.GravatarId.Value;
            if (model.Url.IsSpecified)
                Url = model.Url.Value;
            if (model.Type.IsSpecified)
                Type = model.Type.Value;
            if (model.IsSiteAdmin.IsSpecified)
                IsSiteAdmin = model.IsSiteAdmin.Value;
        }

        public async Task UpdateAsync(RequestOptions options = null)
        {
            var model = await Github.ApiClient.GetUserAsync(Username, options).ConfigureAwait(false);
            Update(model);
        }

        public override string ToString() => Username;
    }
}
